import { useState, useEffect } from 'react';
import './Box.css';
const Box = () => {
    let initialArray = Array(9).fill(null);
    const [squares, setSquares] = useState(initialArray);
    const [isX, setIsX] = useState(true);
    const [win, setWinner] = useState();
    const [move, setMove] = useState('X');


    const handleClick = (index) => {

        if (win || squares[index]) {
            return;
        }
        let arr = squares;
        if (isX) {
            arr[index] = 'X';
            setMove('O');
            setIsX(false);
        } else {
            arr[index] = 'O';
            setMove('X');
            setIsX(true);
        }


        console.log(arr);
        setSquares(arr);



    }
    useEffect(() => {


        console.log(winner(squares));


    }, [squares])
    useEffect(() => {

        let checkFull=squares.every((value)=>{
            return value!=null;
        })
           if(checkFull){
               setMove('');
           }

        if(winner(squares)){
              setWinner(winner(squares)+' is the winner');
              setMove('');
        }

    })

    function winner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return squares[a];
            }
        }
        return null;
    }

    const startGame = () => {
        setSquares(initialArray);
        setWinner();
        setMove('X');
    }
    return (
        <div>
        <div className="winner">{win}</div>
            <div className="squares">

                {
                    squares.map((element, index) => {
                        return (
                            <div className='box' key={index} onClick={() => {
                                handleClick(index);
                            }}>
                                {element}

                            </div>

                        )

                    })
                }
            </div>
            
            <div className="move">move:{move}</div>
            <button onClick={startGame}>New game</button>
        </div>
    )



}

export default Box;